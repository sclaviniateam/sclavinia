#ifndef _GROUND_SPEC_CODES_H
#define _GROUND_SPEC_CODES_H

typedef enum {
  ground_gray_stone,
  ground_brown_stone,
  ground_turf,
  ground_steppe,
  ground_snow,
  ground_earth,
  ground_desert,
  ground_forest,
  ground_pebbles,
  ground_village,
  ground_path,
  ground_wapno,
}Ground_spec_codes;
const int num_ground_specs = 12;



#endif
